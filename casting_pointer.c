#include<stdio.h>
#include<stdint.h>

int main( void){
	uint32_t value = 0xDEADBEEF;
	uint8_t last_byte = (uint8_t)value;
	printf("last byte: %x\n", last_byte);

	uint16_t last_two_bytes = (uint16_t)value;
	printf("last two bytes: %x\n", last_two_bytes);

	uint16_t first_two_bytes = ((uint16_t *)&value)[1];
	printf("first two bytes: %x\n", first_two_bytes);
}

